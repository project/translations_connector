<?php

namespace Drupal\translations_connector\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;

/**
 * Class TranslationsConnector create a form to connect translations.
 *
 * @package Drupal\translations_connector\Form
 * @ingroup translations_connector
 */
class TranslationsConnector extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'translations_connector_form';
  }

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The path alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new TranslationsConnector.
   *
   * @param \Drupal\Core\entity\EntityTypeManagerInterface $entit_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Path\AliasManagerInterface $path_alias_manager
   *   The path alias manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match interface.
   */
  public function __construct(EntityTypeManagerInterface $entit_type_manager, LanguageManagerInterface $language_manager, ModuleHandlerInterface $module_handler, AliasManagerInterface $path_alias_manager, MessengerInterface $messenger, RouteMatchInterface $route_match) {
    $this->entityTypeManager = $entit_type_manager;
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;
    $this->pathAliasManager = $path_alias_manager;
    $this->messenger = $messenger;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('module_handler'),
      $container->get('path_alias.manager'),
      $container->get('messenger'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Current avaliable languages as select list field.
    $languages = $this->languageManager->getLanguages();
    $options = [];

    // Get current node language.
    $nid = $this->routeMatch->getParameter('node');
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    $used_languages = $node->getTranslationLanguages();

    foreach ($languages as $language) {
      if (!array_key_exists($language->getId(), $used_languages)) {
        $options[$language->getId()] = $language->getName();
      }
    }

    if (empty($options)) {
      $form['message'] = [
        '#markup' => $this->t('There are no available languages to add as a translation.'),
      ];
      return $form;
    }

    $form['target_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Target translation language'),
      '#description' => $this->t('Select the language of the translation you want to add.'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    // Linkit field to select node to translate.
    $form['nid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Target node ID'),
      '#description' => $this->t('Enter the node ID of the item in the language above that will be linked as a translation (a node ID consists of 5 digits and can be found in the URL of an item on edit mode).'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateform(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $node = $this->entityTypeManager->getStorage('node')->load($values['nid']);
    $target_nid = $this->routeMatch->getParameter('node');
    $target_node = $this->entityTypeManager->getStorage('node')->load($target_nid);

    if (is_null($node)) {
      $form_state->setErrorByName('nid', $this->t('Node id is not valid'));
      return;
    }

    $used_languages = $node->getTranslationLanguages();

    // Check if bundle is the same.
    if ($node->bundle() != $target_node->bundle()) {
      $form_state->setErrorByName('nid', $this->t("The items you're trying to connect don't belong to the same content type (e.g. item A is a news and item B is a landing page). Please ensure item A and item B are both the same content type."));
      return;
    }

    if (count($used_languages) > 1) {
      $form_state->setErrorByName('nid', $this->t('The translation you tried to add is already connected to another content piece.'));
      return;
    }

    if (!array_key_exists($values['target_language'], $used_languages)) {
      $form_state->setErrorByName('nid', $this->t('This item is not available in the language selected above. Please select the correct language and try again.'));
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Form values.
    $values = $form_state->getValues();

    // Load source node.
    $source_node = $this->entityTypeManager->getStorage('node')->load($values['nid']);

    // Load current node.
    $nid = $this->routeMatch->getParameter('node');
    $node = $this->entityTypeManager->getStorage('node')->load($nid);

    // Get the translatable fields.
    $form_display = EntityFormDisplay::collectRenderDisplay($node, 'default');
    $translatable_fields = array_keys($form_display->getComponents());

    // Get translation values.
    $translation_values = $source_node->toArray();

    // Keep the translatable fields only.
    foreach ($translation_values as $key => $translation_value) {
      if (!in_array($key, $translatable_fields)) {
        unset($translation_values[$key]);
      }
    }

    $translation_values['content_translation_source'] = [$values['target_language'] => $node->langcode->value];

    // Add translation to current node.
    $translation = $node->addTranslation($values['target_language'], $translation_values);

    // Check if layout builder is enabled.
    if ($this->moduleHandler->moduleExists('layout_builder')) {
      if (!is_null($translation->layout_builder__layout)) {
        // Copy layout builder from source node.
        $translation->layout_builder__layout = $source_node->layout_builder__layout;
        $translation->save();
      }
    }

    // Get source node alias.
    $path_alias_manager = $this->entityTypeManager->getStorage('path_alias');
    // Load all path alias for this node for es language.
    $alias_objects = $path_alias_manager->loadByProperties([
      'path' => '/node/' . $values['nid'],
      'langcode' => $values['target_language'],
    ]);

    $alias_object = reset($alias_objects);
    if (!is_null($alias_object)) {
      $alias_object->setPath('/node/' . $nid);
      $alias_object->save();
    }

    // Check if rediret module is enabled.
    if ($this->moduleHandler->moduleExists('redirect')) {
      // Load all redirect entities for source node.
      $redirects = $this->entityTypeManager->getStorage('redirect')->loadByProperties(['redirect_redirect__uri' => 'internal:/node/' . $values['nid']]);
      foreach ($redirects as $redirect) {
        // Change the redirect source to the new node.
        $redirect->setRedirect('/node/' . $nid);
        $redirect->save();
      }
    }

    $source_node->delete();

    // Set a status message.
    $this->messenger->addMessage($translation->getTitle() . '/' . strtoupper($values['target_language']) . ' has been added as a translation of ' . $node->getTitle() . '/' . strtoupper($node->langcode->value) . '.');
  }

}
